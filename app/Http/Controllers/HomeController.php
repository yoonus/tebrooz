<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
//        return 'true';

        $categoriesIcon=[
          'heartIcon.png','NoseIcon.png','eyesIcon.png',
          'ToothIcon.png','hairIcon.png','shortIcon.png',
          'hipIcon.png','breastIcon.png','faceIcon.png'
        ];


        return view('pages.index',compact('categoriesIcon'));
    }
}
