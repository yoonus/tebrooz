<div class="main-title mt-4 mb-3 col-12 float-left">
    <p class="title">الأطباء</p>
    <span class="under-title"></span>
</div>
<div class="swiper-container col-12 float-left doctors-slider p-4">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="doctor-avatar col-12 float-left">
                <img src="{{ asset('images/doctor_1.jpg') }}" alt="">
            </div>
            <div class="doctor-info col-12 float-left p-3">
                <p class="doctor-name">دکتر جولیا جمستون</p>
                <p class="doctor-speciality badge badge-secondary">متخصص پوست و مو</p>
                <p class="description">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد
                </p>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="doctor-avatar col-12 float-left">
                <img src="{{ asset('images/doctor_2.jpg') }}" alt="">
            </div>
            <div class="doctor-info col-12 float-left p-3">
                <p class="doctor-name">دکتر جولیا جمستون</p>
                <p class="doctor-speciality badge badge-secondary">متخصص پوست و مو</p>
                <p class="description">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد
                </p>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="doctor-avatar col-12 float-left">
                <img src="{{ asset('images/doctor_3.jpg') }}" alt="">
            </div>
            <div class="doctor-info col-12 float-left p-3">
                <p class="doctor-name">دکتر جولیا جمستون</p>
                <p class="doctor-speciality badge badge-secondary">متخصص پوست و مو</p>
                <p class="description">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد
                </p>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="doctor-avatar col-12 float-left">
                <img src="{{ asset('images/doctor_4.jpg') }}" alt="">
            </div>
            <div class="doctor-info col-12 float-left p-3">
                <p class="doctor-name">دکتر جولیا جمستون</p>
                <p class="doctor-speciality badge badge-secondary">متخصص پوست و مو</p>
                <p class="description">
                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد
                </p>
            </div>
        </div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>
{{-- <div id="exampleSlider">
    <div class="TitleDoctorSlider">
        <img src="{{ asset('images/title_doctor.png') }}" alt="">
    </div>
    <div class="MS-content">
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_1.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_2.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_3.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_4.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_1.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_2.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_3.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
        <div class="item col-lg-3 col-md-3 col-12">
            <div class="ImageFrame">
                <img src="{{ asset('images/doctor_4.jpg') }}" alt=""/>
            </div>
            <div class="ContentDoctorSlider">
                <span class="doctorName">دکتر جولیا جمستون</span>
                <span class="doctorDegree">متخصص پوست و مو</span>
                <div class="TextDoctorsSlider">
                    دارای بیشتر از چند سال سابقه کار و شرکت در همایش های بین المللی
                </div>
            </div>
        </div>
    </div>
    <div class="MS-controls">
        <button class="MS-left"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
        <button class="MS-right"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
    </div>
</div> --}}

@push('scripts')
    <script type="text/javascript">
        // $('#exampleSlider').multislider({
        //     interval: 4000,
        //     slideAll: false,
        //     continuous:false,
        //     duration: 1500
        // });
    </script>
@endpush