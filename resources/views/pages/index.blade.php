@extends('main')
@section('content')
    <div class="FirstRow section">
        @include('pages.navigation')
        <div class="SliderRow">
            @include('pages.LeftSideSlider')
            @include('pages.slider')
            @include('pages.RightSideSlider')
        </div>
    </div>
    <div class="CategoriesRow section">
        @include('pages.categories')
    </div>
    <div class="DoctorRow section">
        @include('pages.DoctorItems')
    </div>
    {{-- <div class="ArticlesRow section mt-4">
        @include('pages.article')
    </div> --}}
        {{-- 
    <div class="VideoRow">
        @include('pages.video')
    </div>
    <div class="FooterRow">
        @include('pages.footer')
    </div>
    --}}
    
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            var swiper2 = new Swiper.default('.doctors-slider', {
                slidesPerView: 4,
                spaceBetween: 30,
                slidesPerGroup: 4,
                loop: false,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: '.doctors-slider .swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.doctors-slider .swiper-button-next',
                    prevEl: '.doctors-slider .swiper-button-prev',
                },
            }); 
        })
    </script>
@endpush

