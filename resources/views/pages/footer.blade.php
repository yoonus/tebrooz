<div class="FooterColumn">
    <div class="FirstBox">
        <div class="SocialNetWorks">
            <div class="Title">ما را در شبکه های اجتماعی ببینید:</div>
            <div class="Icons">
                <a href="">
                    <i class="fab fa-youtube"></i>
                </a>
                <a href="">
                    <i class="fab fa-twitter-square"></i>
                </a>
                <a href="">
                    <i class="fab fa-instagram"></i>
                </a>
                <a href="">
                    <i class="fab fa-google-plus"></i>
                </a>
                <a href="">
                    <i class="fab fa-facebook-square"></i>
                </a>
            </div>
        </div>
        <div class="TitleServices">خدمات ما در یک نگاه</div>
        <div class="Services">
            <div class="SevicesFrame">
                <span>چشم پزشکی</span>
                <span>-</span>
                <span>فیزیوتراپی</span>
            </div>
            <div class="SevicesFrame">
                <span>دندان پزشکی</span>
                <span>-</span>
                <span>طب سوزنی</span>
            </div>
            <div class="SevicesFrame">
                <span>عمل زیبایی</span>
                <span>-</span>
                <span>کاشت مو</span>
            </div>
        </div>
    </div>
    <div class="SecondBox">
        <span class="Address">مشهد - خیابان هنرور - هنرور 10 - پلاک 20</span>
        <span class="PhoneNumber">دوشنبه 13 آبان 1397</span>
        <span class="date">051-32325687</span>
    </div>
    <div class="ThirdBox">
        <div class="Logo">
            <img src="{{ asset('images/tebRoozLogo.png') }}" alt="">
        </div>
        <div class="About">
            <p>
                طب روز فعالیت خود را در سال 1394 با هدف خدمات هر چه بیشتر به گردشگران عرب زبان آغاز نمود و تاکنون خدمات
                به سزایی را در این زمینه به مشتریان خود ارائه نموده که از...
            </p>
            <a>SIGN IN</a>
        </div>
    </div>
</div>