<div class="main-title mb-3 col-12 float-left">
    <p class="title">مقالات</p>
    <span class="under-title"></span>
</div>
<div class="ArticlesBox">
    <div class="Article col-md-6 col-12 p-3">
        <div class="ImageArticle">
            <img src="{{ asset('images/services_1.jpg') }}" alt="">
        </div>
        <div class="ArticleContent">
            <div class="TXTContent">
                در اکثر کشورهای توسعه یافته، طب سنتی در ابعاد مختلف تحت حمایت دولت قرار گرفته است و سهم خوبی از تأمین سلامت مردم را در سیستم سلامت .....
            </div>
            <div class="ArticleDate">
                25 ربیع الاول
            </div>
        </div>
    </div>
    <div class="Article col-md-6 col-12 p-3">
        <div class="ImageArticle">
            <img src="{{ asset('images/services_2.jpg') }}" alt="">
        </div>
        <div class="ArticleContent">
            <div class="TXTContent">
                در اکثر کشورهای توسعه یافته، طب سنتی در ابعاد مختلف تحت حمایت دولت قرار گرفته است و سهم خوبی از تأمین سلامت مردم را در سیستم سلامت .....
            </div>
            <div class="ArticleDate">
                25 ربیع الاول
            </div>
        </div>
    </div>
    <div class="Article col-md-6 col-12 p-3">
        <div class="ImageArticle">
            <img src="{{ asset('images/services_3.jpg') }}" alt="">
        </div>
        <div class="ArticleContent">
            <div class="TXTContent">
                در اکثر کشورهای توسعه یافته، طب سنتی در ابعاد مختلف تحت حمایت دولت قرار گرفته است و سهم خوبی از تأمین سلامت مردم را در سیستم سلامت .....
            </div>
            <div class="ArticleDate">
                25 ربیع الاول
            </div>
        </div>
    </div>
    <div class="Article col-md-6 col-12 p-3">
        <div class="ImageArticle">
            <img src="{{ asset('images/services_4.jpg') }}" alt="">
        </div>
        <div class="ArticleContent">
            <div class="TXTContent">
                در اکثر کشورهای توسعه یافته، طب سنتی در ابعاد مختلف تحت حمایت دولت قرار گرفته است و سهم خوبی از تأمین سلامت مردم را در سیستم سلامت .....
            </div>
            <div class="ArticleDate">
                25 ربیع الاول
            </div>
        </div>
    </div>
</div>