<div class="RightSideSlider">
    <ul class="SocialNetworkIcon">
       <li>
           <a href="">
               <i class="fab fa-youtube"></i>
           </a>
           <a href="">
               <i class="fab fa-twitter-square"></i>
           </a>
           <a href="">
               <i class="fab fa-instagram"></i>
           </a>
           <a href="">
               <i class="fab fa-google-plus"></i>
           </a>
           <a href="">
               <i class="fab fa-facebook-square"></i>
           </a>
       </li>
    </ul>
    <div class="TebRoozLogo">
        <img src="{{ asset('images/tebRoozLogo.png') }}" alt=""/>
    </div>
    <ul class="ContactNumber">
        <li>
            <i class="fas fa-phone-square-alt"></i>
            <span>09330000188</span>
        </li>
        <li>
            <i class="fas fa-envelope"></i>
            <span>tebrooz@info.com</span>
        </li>
    </ul>
</div>