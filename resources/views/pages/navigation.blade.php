<div class="NavigationRow">
    <div class="NavigationItems SearchIcon">
        <i class="fas fa-search"></i>
    </div>
    <div id="MenuIcon" class="NavigationItems MenuIcon">
        <i class="fas fa-bars"></i>
    </div>
    <nav id="ListMenu" class="NavigationItems ListMenu">
        <ul class="p-0">
            <li><a href="/خانه">خانه</a></li>
            <li><a href="درباره ما">درباره ما</a></li>
            <li><a href="خدمات ما">خدمات ما</a></li>
            <li><a href="مطب ها">مطب ها</a></li>
        </ul>
    </nav>
    <ul class="NavigationItems LanguageFrame">
        <li>
            <a href="/ar">
                <img src="{{ asset('images/araghIconNavigation.png') }}" alt="">
            </a>
        </li>
        <li>
            <a href="/fa">
                <img src="{{ asset('images/iranIconNavigation.png') }}" alt="">
            </a>
        </li>
    </ul>
</div>