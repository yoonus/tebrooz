<!DOCTYPE html>
<html class="rtl" dir="rtl" lang="fa">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!---->
    <meta name="description" content="شرکت تعاونی حمل و نقل سراسری لاجورد اختر کیهان توس در تاریخ 1397/8/3 به شماره ثبت 66058 به شناسه ملی    14007910626  با اهداف توسعه حمل و نقل سراسری، توسعه حمل و نقل جاده ای، ایجاد تعامل بین رانندگان حمل و نقل جاده ای و مالکان خودروهای سنگین و تامین بیمه رانندگان تاسیس شد." />
    <meta name="keywords" content="لاجورد اختر کیهان توس ، حمل و نقل ، حمل و نقل سراسری ، حمل و نقل جاده ای ، ترانزیت کالا ،شرکت تعاونی ، بیمه رانندگان ، حمل و نقل سوخت" />
    <meta name="robots" content="index,nofollow,noarchive" />
    <meta name="expires" content="date" />
    <meta name="rating" content="general" />
    <meta name="resource-type" content="document" />
    <meta name="copyright" content="شرکت تعاونی حمل و نقل جاده ای لاجورد اختر کیهان توس" />
    <meta name="author" content="امید مالک" />
    <!---->

    <link rel="stylesheet" type="text/css" href="{{ asset('multislider/css/custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    @stack('styles')

    <title>@yield('title')</title>
</head>
<body dir="ltr">
    <div class="main-content">
        @yield('content')
    </div>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('multislider/js/multislider.min.js') }}"></script>
    @stack('scripts')
</body>
</html>
